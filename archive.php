<?php
/**
 * Page of archives of posts (archive.php)
 * @package WordPress
 * @subpackage clean-wp-theme-3
 */
get_header(); ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-8 col-md-8">
				<h1 class="page-title"><?php the_archive_title(); ?></h1>
                <h3 class="taxonomy-description"><?php the_archive_description(); ?></h3>
				<?php if (have_posts()) : while (have_posts()) : the_post(); // if there are posts - we launch WP cycle ?>
					<?php get_template_part('template-parts/loop'); // for the mapping of each post we use the template loop.php ?>
				<?php endwhile; // end of cycle ?>
				<?php the_posts_pagination(array(
					'end_size' => 2,
					'mid_size' => 2,
				) ); // page navigation
                else : // If no content, include the "No posts found" template.
                    get_template_part( 'template-parts/content', 'none' );
                endif; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>