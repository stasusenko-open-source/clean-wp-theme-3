<?php
/**
 * Functions template (function.php)
 * @package WordPress
 * @subpackage clean-wp-theme-3
 *
 * Table of Contents
 *
 * 1.0 - Setting basic capabilities
 * 2.0 - Connecting styles in the header
 * 3.0 - Connecting scripts in the footer
 * 4.0 - Connecting frameworks, classes and libraries
 * 5.0 - Posts without title
 * 6.0 - Advanced function the_excerpt
 * 7.0 - Function for comments
 * 8.0 - Registration of widget areas
 */
 
/*----------------------------------------*/
/* 1. 	Setting basic capabilities
/*----------------------------------------*/ 
function cwptheme_setup(){
    /* Thumbnails */
	add_theme_support('post-thumbnails'); // adding support for thumbnails
	set_post_thumbnail_size(250, 150); // setting the size of thumbnails 250x150
	add_image_size('big-thumb', 400, 400, true); // adding one more dimension to 400x400 pictures with cropping
	/* Link to the PCC pages and posts in the section HEAD */
	add_theme_support( 'automatic-feed-links' );
    /* This feature allows plugins and themes to change the meta tag <title> */
    add_theme_support( 'title-tag' );
	/*  */
	add_editor_style( '/css/main.css' );
    /* Add custom background and custom image for header*/
    add_theme_support( 'custom-background', array('default-color' => 'f7f8fb') );
	add_theme_support( 'custom-header', array('width' => 1920, 'height' => 300, 'header-text' => false,) );
	/* Menu */
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'clean-wp-theme-3' ),
	) );
	/* Woocommerce support declaration */
	add_theme_support( 'woocommerce' );
	/* Internationalization */
	load_theme_textdomain('clean-wp-theme-3', get_template_directory() . ('/languages'));
}
add_action('after_setup_theme', 'cwptheme_setup');

/* Width for embedded video */
if ( ! isset( $content_width ) ) $content_width = 750;

/*----------------------------------------*/
/* 2. 	Connecting styles in the header
/*----------------------------------------*/
function cwptheme_add_styles() {
    // reset browser styles
    wp_enqueue_style('normalize', get_theme_file_uri('/css/normalize.css'), array(), null);
    // grid Bootstrap
    wp_enqueue_style('grid', get_theme_file_uri('/css/grid.css'), array(), null);
    // main theme styles
    wp_enqueue_style( 'parent-main-style', get_theme_file_uri('/css/main.css'), array(), null);
}
add_action('wp_print_styles', 'cwptheme_add_styles');

/*-----------------------------------------*/
/* 3.	Connecting scripts in the footer
/*-----------------------------------------*/
function cwptheme_add_scripts() {
    // FitVids
    wp_enqueue_script('jquery.fitvids', get_theme_file_uri('/js/fitvids/jquery.fitvids.js'), array(), null);
    wp_enqueue_script('fitvids', get_theme_file_uri('/js/fitvids/init.fitvids.js'), array(), null);
}
add_action('wp_footer', 'cwptheme_add_scripts');

/*--------------------------------*/
/* 3.1 	Connecting Google Fonts
/*--------------------------------*/
function cwptheme_add_google_fonts() {
    wp_enqueue_style( 'custom-google-fonts', 'https://fonts.googleapis.com/css?family=Ubuntu:400,500,700&amp;subset=cyrillic-ext,greek-ext,latin-ext', false, null );
}
add_action( 'wp_enqueue_scripts', 'cwptheme_add_google_fonts' );

/*------------------------------------------------------*/
/* 4. 	Connecting frameworks, classes and libraries
/*------------------------------------------------------*/

/*------------------------------------------------------*/
/* 4.1 	Integration Woocommerce
/*------------------------------------------------------*/
function cwptheme_wrapper_start() {
    echo '<div class="container"><div class="row"><div class="col-md-8">';
}
function cwptheme_wrapper_end() {
    echo '</div>';
}
function cwptheme_woocommerce_sidebar() {
    get_sidebar();
    echo '</div></div>';
}
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
add_action('woocommerce_before_main_content', 'cwptheme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'cwptheme_wrapper_end', 10);
add_action('woocommerce_sidebar', 'cwptheme_woocommerce_sidebar', 10);

/*----------------------------------------*/
/* 5. 	Title for posts without title :)
/*----------------------------------------*/
function filter_the_title( $title, $id ) {
    if ( empty( $title ) ) {
        return __( 'This post has an empty title. Add a title in the admin panel.', 'clean-wp-theme-3');
    } else {
        return $title;
    }
};
add_filter( 'the_title', 'filter_the_title', 10, 2 );

/*--------------------------------------*/
/* 6. 	Advanced function the_excerpt
/*--------------------------------------*/
function cwptheme_excerpt( $args = '' ){
    global $post;
    $default = array(
        'maxchar'   => 150,   // количество символов.
        'text'      => '',    // какой текст обрезать (по умолчанию post_excerpt, если нет post_content.
        // Если есть тег <!--more-->, то maxchar игнорируется и берется все до <!--more--> вместе с HTML
        'autop'     => true,  // Заменить переносы строк на <p> и <br> или нет
        'save_tags' => '<strong><b><a>',    // Теги, которые нужно оставить в тексте, например '<strong><b><a>'
        'more_text' => __('Read more...', 'clean-wp-theme-3'), // текст ссылки для тэга <!--more-->
    );
    if( is_array($args) ) $_args = $args;
    else                  parse_str( $args, $_args );
    $rg = (object) array_merge( $default, $_args );
    if( ! $rg->text ) $rg->text = $post->post_excerpt ?: $post->post_content;
    $rg = apply_filters('cwptheme_excerpt_args', $rg );
    $text = $rg->text;
    $text = preg_replace ('~\[/?.*?\](?!\()~', '', $text ); // remove shortcodes, example:[singlepic id=3], markdown +
    $text = trim( $text );
    // <!--more-->
    if( strpos( $text, '<!--more-->') ){
        preg_match('/(.*)<!--more-->/s', $text, $mm );
        $text = trim($mm[1]);
        $text_append = ' <a href="'. get_permalink( $post->ID ) .'#more-'. $post->ID .'">'. $rg->more_text .'</a>';
    }
    // text, excerpt, content
    else {
        $text = trim( strip_tags($text, $rg->save_tags) );
        // Crop
        if( mb_strlen($text) > $rg->maxchar ){
            $text = mb_substr( $text, 0, $rg->maxchar );
            $text = preg_replace('~(.*)\s[^\s]*$~s', '\\1 ... <a href="'. get_permalink() .'"><b>' . _e('Read more', 'clean-wp-theme-3') . '</b></a>', $text ); // remove last word
        }
    }
    // A simple analog wpautop()
    if( $rg->autop ){
        $text = preg_replace(
            array("~\r~", "~\n{2,}~", "~\n~",   '~</p><br ?/>~'),
            array('',     '</p><p>',  '<br />', '</p>'),
            $text
        );
    }
    $text = apply_filters('cwptheme_excerpt', $text, $rg );
    if( isset($text_append) ) $text .= $text_append;
    return ($rg->autop && $text) ? "<p>$text</p>" : $text;
}

/*------------------------------------------*/
/* 7. 	Function for comments
/*------------------------------------------*/

/*------------------------------------------*/
/* 7.1 	Walker comment
/*------------------------------------------*/
class clean_comments_constructor extends Walker_Comment { // класс, который собирает всю структуру комментов
    public function start_lvl( &$output, $depth = 0, $args = array()) { // что выводим перед дочерними комментариями
        $output .= '<ul class="children">' . "\n";
    }
    public function end_lvl( &$output, $depth = 0, $args = array()) { // что выводим после дочерних комментариев
        $output .= "</ul><!-- .children -->\n";
    }
    protected function comment( $comment, $depth, $args ) { // разметка каждого комментария, без закрывающего </li>!
        $classes = implode(' ', get_comment_class()).($comment->comment_author_email == get_the_author_meta('email') ? ' author-comment' : ''); // берем стандартные классы комментария и если коммент пренадлежит автору поста добавляем класс author-comment
        echo '<li id="comment-'.get_comment_ID().'" class="'.$classes.' media">'."\n"; // родительский тэг комментария с классами выше и уникальным якорным id
        echo '<div class="media-left">'.get_avatar($comment, 64, '', get_comment_author(), array('class' => 'media-object'))."</div>\n"; // покажем аватар с размером 64х64
        echo '<div class="media-body">';
        echo '<span class="meta media-heading">Автор: '.get_comment_author()."\n"; // имя автора коммента
        //echo ' '.get_comment_author_email(); // email автора коммента, плохой тон выводить почту
        echo ' '.get_comment_author_url(); // url автора коммента
        echo ' Добавлено '.get_comment_date('F j, Y в H:i')."\n"; // дата и время комментирования
        if ( '0' == $comment->comment_approved ) echo '<br><em class="comment-awaiting-moderation">Ваш комментарий будет опубликован после проверки модератором.</em>'."\n"; // если комментарий должен пройти проверку
        echo "</span>";
        comment_text()."\n"; // текст коммента
        $reply_link_args = array( // опции ссылки "ответить"
            'depth' => $depth, // текущая вложенность
            'reply_text' => 'Ответить', // текст
            'login_text' => 'Вы должны быть залогинены' // текст если юзер должен залогинеться
        );
        echo get_comment_reply_link(array_merge($args, $reply_link_args)); // выводим ссылку ответить
        echo '</div>'."\n"; // закрываем див
    }
    public function end_el( &$output, $comment, $depth = 0, $args = array() ) { // конец каждого коммента
        $output .= "</li><!-- #comment-## -->\n";
    }
}

/*------------------------------------------*/
/* 7.2 	For thread comments
/*------------------------------------------*/
function cwptheme_enqueue_comments_reply() {
	if( get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'comment_form_before', 'cwptheme_enqueue_comments_reply' );

/*--------------------------------------*/
/* 8.	Registration of widget areas
/*--------------------------------------*/
function cwptheme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'clean-wp-theme-3' ),
		'id'            => 'sidebar',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'clean-wp-theme-3' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<span class="widget-title">',
		'after_title'   => '</span>',
	) );
}
add_action( 'widgets_init', 'cwptheme_widgets_init' );