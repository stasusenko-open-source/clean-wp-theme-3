<?php
/**
 * Sidebar template (sidebar.php)
 * @package WordPress
 * @subpackage clean-wp-theme-3
 */
?>
<div class="col-xs-12 col-md-4">
    <aside class="sidebar">
        <?php dynamic_sidebar('sidebar'); // display the sidebar ?>
    </aside>
</div>