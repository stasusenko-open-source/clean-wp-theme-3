<?php
/**
 * Шаблон коментарів (comments.php)
 * Виводить список коментарів та форму для додавання коментарів
 * @package WordPress
 * @subpackage clean-wp-theme-3
 */
?>
<div id="comments"> <?php // div з цим id потрібен для якірних посилань на коментарі ?>
	<h2>Всього коментарів: <?php echo get_comments_number(); // загальна кількість коментарів ?></h2>
	<?php if (have_comments()) : // якщо коментарі є - початок ?>
	<ul class="comment-list media-list">
		<?php
			$args = array( // аргументи для списку коментарів (деякі опції налаштовуються у панелі адміністратора, а інші у класі clean_comments_constructor)
				'walker' => new clean_comments_constructor, // клас, який збирає всю структуру коментарів (знаходиться у function.php)
			);
			wp_list_comments($args); // відображаємо коментарі
		?>
	</ul>
		<?php if (get_comment_pages_count() > 1 && get_option( 'page_comments')) : // якщо сторінок з коментарями більше однієї та посторінкова навігація ввімкнена - початок ?>
		<?php $args = array( // аргументи для посторінкової навігації
				'prev_text' => '«', // текст назад
				'next_text' => '»', // текст вперед
				'type' => 'array',
				'echo' => false
			); 
			$page_links = paginate_comments_links($args); // виводимо посторінкову навігацію
			if( is_array( $page_links ) ) { // якщо посторінкова навігація є
			    echo '<ul class="pagination comments-pagination">';
			    foreach ( $page_links as $link ) {
			    	if ( strpos( $link, 'current' ) !== false ) echo "<li class='active'>$link</li>"; // якщо це активна сторінка
			        else echo "<li>$link</li>"; 
			    }
			   	echo '</ul>';
		 	}
		?>
		<?php endif; // якщо сторінок з коментарями більше однієї та посторінкова навігація ввімкнена - кінець ?>
	<?php endif; // якщо коментарі є - кінець ?>
	<?php if (comments_open()) { // якщо коментування ввімкнено для даного запису
		/* ФОРМА КОМЕНТУВАННЯ */
		$fields =  array( // розмітка текстових полів форми
			'author' => '<div class="form-group"><label for="author">Ім\'я</label><input class="form-control" id="author" name="author" type="text" value="'.esc_attr($commenter['comment_author']).'" size="30" required></div>', // поле Ім'я
			'email' => '<div class="form-group"><label for="email">Email</label><input class="form-control" id="email" name="email" type="email" value="'.esc_attr($commenter['comment_author_email']).'" size="30" required></div>', // поле email
			'url' => '<div class="form-group"><label for="url">Сайт</label><input class="form-control" id="url" name="url" type="text" value="'.esc_attr($commenter['comment_author_url']).'" size="30"></div>', // поле сайт
			);
		$args = array( // опції форми коментування
			'fields' => apply_filters('comment_form_default_fields', $fields), // змінюємо стандартні поля на поля з масиву вище ($fields)
			'comment_field' => '<div class="form-group"><label for="comment">Коментар:</label><textarea class="form-control" id="comment" name="comment" cols="45" rows="8" required></textarea></div>', // розмітка поля для коментування
			'must_log_in' => '<p class="must-log-in">Ви маєте бути зареєстровані! '.wp_login_url(apply_filters('the_permalink',get_permalink())).'</p>', // текст "Ви маєте бути зареєстровані!"
			'logged_in_as' => '<p class="logged-in-as">'.sprintf(__( 'Ви увійшли як <a href="%1$s">%2$s</a>. <a href="%3$s">Выйти?</a>', 'clean-wp-theme-3'), admin_url('profile.php'), $user_identity, wp_logout_url(apply_filters('the_permalink',get_permalink()))).'</p>', // розмітка "Ви увійшли як"
			'comment_notes_before' => '<p class="comment-notes">Ваш email не буде опублікований.</p>', // Текст до форми
			'comment_notes_after' => '<p class="help-block form-allowed-tags">'.sprintf(__( 'Ви маєте можливість використовувати наступні <abbr>HTML</abbr> теги: %s', 'clean-wp-theme-3'),'<code>'.allowed_tags().'</code>').'</p>', // текст після форми
			'id_form' => 'commentform', // атрибут id форми
			'id_submit' => 'submit', // атрибут id кнопки відправити
			'title_reply' => 'Залишити коментар', // заголовок форми
			'title_reply_to' => 'Відповісти %s', // текст "Відповісти"
			'cancel_reply_link' => 'Відмінити відповідь', // текст "Відмінити відповідь"
			'label_submit' => 'Відправити', // Текст на кнопці відправити
			'class_submit' => 'btn btn-default' // новий параметр з класом кнопки, доданий з WP 4.1
		);
	    comment_form($args); // відображаємо форму
	} ?>
</div>