<?php
/**
 * Template for search form (searchform.php)
 * @package WordPress
 * @subpackage clean-wp-theme-3
 */
?>
<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
	<div class="form-group">
		<input id="search-field" type="search" placeholder="<?php _e('Search field', 'clean-wp-theme-3'); ?>" value="<?php echo get_search_query() ?>" name="s">
	</div>
	<button type="submit"><?php _e('Go!', 'clean-wp-theme-3'); ?></button>
</form>