<?php
/**
 * Author page (author.php)
 * @package WordPress
 * @subpackage clean-wp-theme-3
 */
get_header(); ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8">
			    <h1 class="page-title"><?php the_archive_title(); // with name or nickname, depending on the author's choice ?></h1>
				<div class="author-info">
					<div class="author-avatar">
                        <?php echo get_avatar( get_the_author_meta('user_email'), 64 ); // user picture ?>
					</div>
                    <div class="author-details">
                        <p><?php the_author_meta('user_url') // address of the author's web-site, if there is any ?></p>
                        <p><?php the_author_meta('description'); // description, if there is any ?></p>
                    </div>
				</div>
				<?php if (have_posts()) : while (have_posts()) : the_post(); // if there are posts - we launch WP cycle ?>
					<?php get_template_part('template-parts/loop'); // for the mapping of each post we use the template loop.php ?>
				<?php endwhile; endif; // end of cycle ?>
				<?php the_posts_pagination(array(
					'end_size' => 2,
					'mid_size' => 2,
				) ); // page navigation ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>