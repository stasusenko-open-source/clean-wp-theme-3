<?php
/**
 * 404 error page (404.php)
 * @package WordPress
 * @subpackage clean-wp-theme-3
 */
get_header(); ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8">
                <h1 class="page-title"><?php _e('It\'s too bad! There is no such page.', 'clean-wp-theme-3'); ?></h1>
				<p><?php _e('It looks like nothing was found at this location. Maybe try a search?','clean-wp-theme-3')?></p>
				<?php get_search_form(); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>