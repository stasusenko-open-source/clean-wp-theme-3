<?php
/**
 * Search template (search.php)
 * @package WordPress
 * @subpackage clean-wp-theme
 */
get_header(); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12  col-md-offset-2 col-md-8">
                <?php if ( have_posts() ) : ?>
                    <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'clean-wp-theme' ), '<span>' . get_search_query() . '</span>' ); // search title ?></h1>
                <?php else : ?>
                    <h1 class="page-title"><?php _e( 'Nothing Found', 'clean-wp-theme' ); // search title ?></h1>
                <?php endif; ?>

                <?php if (have_posts()) : while (have_posts()) : the_post(); // if there are posts - we launch WP cycle ?>
                    <?php get_template_part('template-parts/loop'); // for the mapping of each post we use the template loop.php ?>
                <?php endwhile;
                else: ?>
                    <p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'clean-wp-theme' ); ?></p>
                    <?php get_search_form(); ?>
                <?php endif; // end of cycle ?>

                <?php the_posts_pagination(array(
                    'end_size' => 2,
                    'mid_size' => 2,
                ) ); // page navigation ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>