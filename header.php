<?php
/**
 * Header teplate (header.php)
 * @package WordPress
 * @subpackage clean-wp-theme-3
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta content='true' name='HandheldFriendly'/>
	<meta content='width' name='MobileOptimized'/>
    <meta content='yes' name='apple-mobile-web-app-capable'/>
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
<div class="wide"><!-- Website style: in a wrapper or without it (class boxed or wide) -->
	<div class="all">
		<nav>
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<?php $args = array( // опції для відображення меню, щоб вони працювали, меню повинно бути створене у панелі адміністратора
							'theme_location' => 'top', // ідентифікатор меню, що визначений у register_nav_menus() в function.php
							'container'=> false, // list wrapper, false - it's nothing
							'menu_class' => '', // class for ul
							'menu_id' => '', // id for ul
							'fallback_cb' => false
						);
						wp_nav_menu($args); // display the menu
						?>
					</div>
				</div>
			</div>
		</nav>
		
		<header class="site-header">
            <img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
            <div class="container-fluid">
                <h1 class="site-title"><?php bloginfo('name'); ?></h1>
            </div>
		</header>