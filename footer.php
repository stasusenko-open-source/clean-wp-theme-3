<?php
/**
 * Footer template (footer.php)
 * @package WordPress
 * @subpackage clean-wp-theme-3
 */
?>
	</div><!-- /all -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<p>&copy; <?php echo date( 'Y' ); ?> <?php bloginfo('name'); ?>. <?php _e('Powered by WordPress', 'clean-wp-theme-3'); ?>. SQL - <?php echo get_num_queries (); ?> | <?php timer_stop (1); ?> sec. | <?php echo round (memory_get_usage ()/1024/1024, 2) ?> MB</p>
				</div>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
</div><!-- /Website style: in a wrapper or without it (class boxed or wide) -->
</body>
</html>