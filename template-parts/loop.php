<?php
/**
 * Post in a cycle (loop.php)
 * @package WordPress
 * @subpackage clean-wp-theme-3
 */ 
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('fix-width-article'); ?>>
	<div class="col-xs-12">
		<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
		<div class="meta">
			<p><?php _e('Posted:', 'clean-wp-theme-3'); ?> <?php the_time(get_option('date_format')); ?> <?php _e('at', 'clean-wp-theme-3'); ?> <?php the_time( get_option('time_format') ); // date and time of publication ?></p>
			<p><?php _e('Author:', 'clean-wp-theme-3'); ?> <?php the_author_posts_link(); // the name of the author with link to all his posts ?></p>
			<p><?php _e('Category:', 'clean-wp-theme-3'); ?> <?php the_category(', ') // categories comma separated ?></p>
            <p><?php the_tags(); ?></p>
		</div>
	</div>
    <div class="col-xs-12">
        <?php if ( has_post_thumbnail() ) { ?>
            <div class="col-xs-12 col-md-4">
                <a href="<?php the_permalink(); ?>" class="thumbnail">
                    <?php the_post_thumbnail(); ?>
                </a>
            </div>
        <?php } ?>
        <div class="<?php if ( has_post_thumbnail() ) { ?>col-xs-12 col-md-8<?php } else { ?>col-xs-12 col-md-12<?php } // різні класи в залежності від того чи є мініатюра ?>">
            <?php echo cwptheme_excerpt( array('maxchar'=>160) ); // функція з functions.php, що обрізає анонс з точністю до слова та зберігає HTML-теги ?>
        </div>
    </div>
</article>