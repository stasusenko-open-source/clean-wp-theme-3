<?php
/**
 * Template part for displaying a message that posts cannot be found (content-none.php)
 * @package WordPress
 * @subpackage clean-wp-theme
 */
?>
<?php
if ( current_user_can( 'publish_posts' ) ) : ?>
    <p><?php printf( __( 'There are currently no posts here. Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'clean-wp-theme' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>
<?php else : ?>
    <p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'clean-wp-theme' ); ?></p>
    <?php get_search_form();
endif; ?>