<?php
/**
 * Standard page template (page.php)
 * @package WordPress
 * @subpackage clean-wp-theme-3
 */
get_header(); ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); // start WP cycle ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> <?php // container with classes and id ?>
						<h1 class="page-title"><?php the_title(); // page-title ?></h1>
						<?php the_content(); // content ?>
					</article>
				<?php endwhile; // end WP cycle ?>
                <?php if (comments_open() || get_comments_number()) comments_template('', true); // if the comment is allowed - we list display the comments and the form for commenting ?>
            </div>
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>