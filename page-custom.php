<?php
/**
 * Page with custom template (page-custom.php)
 * @package WordPress
 * @subpackage clean-wp-theme-3
 * Template Post Type: post, page, product
 <!--?php <br ?--> /*
 Template Name: Custom template
 */
__( 'Custom template', 'clean-wp-theme-3' );
get_header(); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); // start WP cycle ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> <?php // container with classes and id ?>
                        <h1 class="page-title"><?php the_title(); // page-title ?></h1>
						<?php the_content(); // content ?>
                    </article>
				<?php endwhile; // end WP cycle ?>
            </div>
			<?php get_sidebar(); ?>
        </div>
    </div>
<?php get_footer(); ?>