<?php
/**
 * Single entry template (single.php)
 * @package WordPress
 * @subpackage clean-wp-theme-3
 */
get_header(); ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); // start of cycle ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<h1><?php the_title(); // title ?></h1>
						<div class="meta">
							<p><?php _e('Posted:', 'clean-wp-theme-3'); ?> <?php the_time( get_option('date_format') ); ?> <?php _e('at', 'clean-wp-theme-3'); ?> <?php the_time( get_option('time_format') ); // date and time of publication ?></p>
							<p><?php _e('Author:', 'clean-wp-theme-3'); ?> <?php the_author_posts_link(); // the name of the author with link to all his posts ?></p>
							<p><?php _e('Category:', 'clean-wp-theme-3'); ?> <?php the_category(', ') // categories comma separated ?></p>
                            <p><?php the_tags(); ?></p>
						</div>
						<?php the_content(); // content ?>
					</article>
                    <?php $args = array(
                    'before'           => '<p>' . __('Pages:', 'clean-wp-theme-3'),
                    'after'            => '</p>',
                    'link_before'      => '',
                    'link_after'       => '',
                    'next_or_number'   => 'number',
                    'nextpagelink'     => __('Next page', 'clean-wp-theme-3'),
                    'previouspagelink' => __('Previous page', 'clean-wp-theme-3'),
                    'pagelink'         => '%',
                    'echo'             => 1,
                    );

                    wp_link_pages( $args ); ?>
				<?php endwhile; // end of cycle ?>
                <div class="col-xs-12">
                    <?php the_post_navigation( array(
                        'next_text' => __('Next entry:', 'clean-wp-theme-3') . ' %title',
                        'prev_text' => __('Previous entry:', 'clean-wp-theme-3') . ' %title',
                    ) ); // link to previous and next entry ?>
                    <?php if (comments_open() || get_comments_number()) comments_template('', true); // if the comment is allowed - we list display the comments and the form for commenting ?>
                </div>
            </div>
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>